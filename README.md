/MrStickman/

Small shoot 'em up where you control a turret with the left and right arrow keys and shoot up incoming space gems via the spacebar. They drop pickups like different weapons and bullet time. Don't get below 0 points. You lose points when a gem disappears BELOW you or when one hits your turret. Gaining points is done via special pickups, shooting up gems or shooting gems in a way, so that they get ejected out of the game's screen. Don't forget to use your tractor beam with the 'S' key to pull in pick ups. But be careful not to pull in gems as well.

(* 000 - Add README.md.)
* 001 - First commit with full Unity project.