using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerMotor : MonoBehaviour
{
    [SerializeField] private float       _maxMoveSpeed    = 14f;
    [SerializeField] private float       _acceleration    = 9.5f;
    [SerializeField] private float       _deceleration    = 21.5f;
    [SerializeField] private float       _jumpForce       = 10f;
    [SerializeField] private float       _flyForce        = 12f;
    [SerializeField] private float       _maxFlyYVelocity = 5f;
    [SerializeField] private Transform[] _groundCheckRaycasts;
    [SerializeField] private LayerMask   _groundCheckRayLayers;
    [SerializeField] private float       _groundCheckRayDistance = 0.12f;

    public BasePickup ActivePickup             { get; private set; }
    public float      CurrentActivePickupTimer { get; private set; }
    public float      CurrentMoveSpeed         { get; private set; }
    public bool       IsFlyingAllowed          { get; private set; }

    private bool           _isGrounded;
    private bool           _isJumping;
    private bool           _doubleJumpedAlready;
    private float          _currentMoveDir;
    private float          _lastFramesMoveDir;
    private bool           _isDoubleJumpAllowed;
    private Rigidbody2D    _rb;
    private PlayerAnimator _anim;


    public void ActivatePickup(BasePickup newPickup, float duration)
    {
        ActivePickup             = newPickup;
        CurrentActivePickupTimer = duration;
    }


    public void Move(float moveDir) => _currentMoveDir = moveDir;


    public void Jump()
    {
        if (_isGrounded || _isDoubleJumpAllowed && !_doubleJumpedAlready)
        {
            if (!_isGrounded) // If we get in this condition even though we are not grounded, than that means we just did a double jump
                _doubleJumpedAlready = true;
            _rb.AddForce(Vector3.up * _jumpForce, ForceMode2D.Impulse);
            _isJumping = true;
        }
    }


    public void Fly()
    {
        _rb.AddForce(Vector3.up * _flyForce, ForceMode2D.Force);
        Vector2 vel = _rb.velocity;
        _rb.velocity = new Vector2(vel.x, Mathf.Clamp(vel.y, -50f, _maxFlyYVelocity));
        print("velocity: " + _rb.velocity);
    }


    public void AllowDoubleJump(float pickupEffectDuration)
    {
        _isDoubleJumpAllowed = true;
        Invoke(nameof(DisallowDoubleJump), pickupEffectDuration);
    }


    public void AllowFlying(float pickupEffectDuration)
    {
        IsFlyingAllowed = true;
        Invoke(nameof(DisallowFlying), pickupEffectDuration);
    }


    private void Start()
    {
        _rb   = GetComponent<Rigidbody2D>();
        _anim = GetComponent<PlayerAnimator>();
    }


    private void Update()
    {
        CheckGround();

        if (_currentMoveDir != 0f)
        {
            CurrentMoveSpeed = Mathf.Clamp(CurrentMoveSpeed + _acceleration * Time.deltaTime, 0f, _maxMoveSpeed);

            transform.position = new Vector3(transform.position.x + _currentMoveDir * CurrentMoveSpeed * Time.deltaTime, transform.position.y, transform.position.z);

            _lastFramesMoveDir = _currentMoveDir;
        }
        else
        {
            CurrentMoveSpeed   = Mathf.Clamp(CurrentMoveSpeed     - _deceleration                         * Time.deltaTime, 0f, _maxMoveSpeed);
            transform.position = new Vector3(transform.position.x + _lastFramesMoveDir * CurrentMoveSpeed * Time.deltaTime, transform.position.y, transform.position.z);
        }

        _anim?.UpdateAnimations(_currentMoveDir, _isJumping);

        _currentMoveDir = 0f; // Reset for the next frame

        if (ActivePickup)
        {
            CurrentActivePickupTimer -= Time.deltaTime;
            if (CurrentActivePickupTimer <= 0f)
                ActivePickup = null;
        }
    }


    private void CheckGround()
    {
        _isGrounded = false;
        foreach (Transform raycastCheckLocation in _groundCheckRaycasts)
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(raycastCheckLocation.position, Vector2.down, _groundCheckRayDistance, _groundCheckRayLayers);
            if (hitInfo.collider)
            {
                _isGrounded          = true;
                _isJumping           = false;
                _doubleJumpedAlready = false;
                break;
            }
        }
    }


    private void DisallowDoubleJump()
    {
        _isDoubleJumpAllowed = false;
        _doubleJumpedAlready = false;
    }


    private void DisallowFlying() => IsFlyingAllowed = false;
}