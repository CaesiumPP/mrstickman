﻿using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField] [Tooltip("The lower this value is, the farther it will zoom out on higher velocities!")] private float _moveSpeedDividend = 2.5f;

    private Camera      _mainCam;
    private PlayerMotor _playerMotor;
    private float       _originalOrthoSize;


    private void Start()
    {
        _mainCam           = Camera.main;
        _playerMotor       = GetComponent<PlayerMotor>();
        _originalOrthoSize = _mainCam.orthographicSize;
    }


    private void LateUpdate()
    {
        // Cam follow
        float maxZ = 3f;
        _mainCam.transform.position = new Vector3(transform.position.x, transform.position.y, _mainCam.transform.position.z);

        // Zoom
        _mainCam.orthographicSize = _originalOrthoSize + _playerMotor.CurrentMoveSpeed / _moveSpeedDividend;
    }
}