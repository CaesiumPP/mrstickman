using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimator : MonoBehaviour
{
    [SerializeField] private string _moveAnimKey = "Move";
    [SerializeField] private string _jumpAnimKey = "Jump";

    private Animator _animator;


    public void UpdateAnimations(float moveDir, bool isJumping)
    {
        // moveDir is exactly 0f, -1f or 1f at this point!
        bool isMoving = moveDir != 0f;
        _animator.SetBool(_moveAnimKey, isMoving);

        if (isMoving)
        {
            Vector3 localScale         = transform.localScale;
            float   newStickmanLookDir = moveDir < 0f ? Mathf.Abs(localScale.x) * -1f : Mathf.Abs(localScale.x);
            transform.localScale = new Vector3(newStickmanLookDir, localScale.y, localScale.z);
        }

        _animator.SetBool(_jumpAnimKey, isJumping);
    }


    private void Start() => _animator = GetComponent<Animator>();
}