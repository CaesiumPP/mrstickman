﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private Text _activePickupTxt;
    [SerializeField] private Text _activePickupDurationTxt;
    [SerializeField] private Text _currentMoveSpeedTxt;


    private PlayerMotor _playerMotor;


    private void Start()
    {
        _playerMotor = FindObjectOfType<PlayerMotor>();
    }


    private void Update()
    {
        BasePickup activePickup = _playerMotor.ActivePickup;
        _activePickupTxt.enabled         = activePickup;
        _activePickupDurationTxt.enabled = activePickup;

        if (activePickup)
        {
            _activePickupTxt.text         = $"CURRENT ACTIVE PICKUP: {activePickup.PickupName}";
            _activePickupDurationTxt.text = $"TIME LEFT: {_playerMotor.CurrentActivePickupTimer}";
        }

        _currentMoveSpeedTxt.text = $"CURRENT SPEED: {_playerMotor.CurrentMoveSpeed}";
    }
}