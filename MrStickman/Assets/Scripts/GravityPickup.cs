using UnityEngine;

public class GravityPickup : BasePickup
{
    [SerializeField] private float _newGravityToApply = Mathf.Abs(GRAVITY2D);

    private const float GRAVITY2D = -9.81f;


    protected override void ApplyPickupEffect(PlayerMotor playerMotor)
    {
        base.ApplyPickupEffect(playerMotor);
        Physics2D.gravity = new Vector2(0f, _newGravityToApply);
    }


    private void OnDestroy() => Physics2D.gravity = new Vector2(0f, GRAVITY2D);
}