public class DoubleJumpPickup : BasePickup
{
    protected override void ApplyPickupEffect(PlayerMotor playerMotor)
    {
        base.ApplyPickupEffect(playerMotor);
        playerMotor.AllowDoubleJump(_pickupEffectDuration);
    }
}