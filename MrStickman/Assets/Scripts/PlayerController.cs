﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(PlayerCamera))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private string _jumpAndFlyInputKey = "Jump";
    [SerializeField] private string _moveAxisInputKey   = "Horizontal";


    private PlayerMotor _motor;


    private void Start() => _motor = GetComponent<PlayerMotor>();


    private void Update()
    {
        if (!_motor) return;

        float       moveDir         = Input.GetAxis(_moveAxisInputKey);
        const float INPUT_TOLERANCE = 0.05f;
        if (moveDir < -INPUT_TOLERANCE)
            moveDir = -1f;
        else if (moveDir > INPUT_TOLERANCE)
            moveDir = 1f;
        else
            moveDir = 0f;

        _motor.Move(moveDir);

        if (_motor.IsFlyingAllowed)
        {
            if (Input.GetButton(_jumpAndFlyInputKey))
                _motor.Fly();
        }
        else
        {
            if (Input.GetButtonDown(_jumpAndFlyInputKey))
                _motor.Jump();
        }
    }
}