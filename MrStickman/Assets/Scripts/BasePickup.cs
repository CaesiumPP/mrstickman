using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class BasePickup : MonoBehaviour
{
    [SerializeField] protected float  _pickupEffectDuration = 7f;
    [SerializeField] protected string _pickupName;

    public string PickupName => _pickupName;

    protected bool _effectAppliedAlready;


    protected virtual void Start() => GetComponent<Collider2D>().isTrigger = true;


    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (_effectAppliedAlready) return;

        if (other.CompareTag("Player"))
        {
            PlayerMotor playerMotor = other.GetComponent<PlayerMotor>();
            if (playerMotor.ActivePickup) return; // Already one active? Don't do anything!
            ApplyPickupEffect(playerMotor);
            GetComponent<SpriteRenderer>().enabled = false;
            _effectAppliedAlready                  = true;
            Destroy(gameObject, _pickupEffectDuration);
        }
    }


    protected virtual void ApplyPickupEffect(PlayerMotor playerMotor)
    {
        playerMotor.ActivatePickup(this, _pickupEffectDuration);
    }
}