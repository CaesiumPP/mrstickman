public class FlyPickup : BasePickup
{
    protected override void ApplyPickupEffect(PlayerMotor playerMotor)
    {
        base.ApplyPickupEffect(playerMotor);
        playerMotor.AllowFlying(_pickupEffectDuration);
    }
}